# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

{
  outputs =
    { self
    , nixpkgs
    , flake-utils
    }:
    flake-utils.lib.eachDefaultSystem (
      system:
      let
        pkgs = import nixpkgs { inherit system; };
      in
      {
        devShells.default = import ./shell.nix { inherit pkgs; };
        packages = rec {
          default = anyrun-files;
          anyrun-files = pkgs.callPackage ./package.nix { };
        };
        formatter = pkgs.nixpkgs-fmt;
      }
    );
}
