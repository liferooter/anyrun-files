# SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
#
# SPDX-License-Identifier: CC0-1.0

{ pkgs ? import <nixpkgs> { } }:
let
  inherit (pkgs) lib;
  cargoToml = with builtins; fromTOML (readFile ./Cargo.toml);
in
pkgs.rustPlatform.buildRustPackage {
  pname = cargoToml.package.name;

  inherit (cargoToml.package) version;

  src = ./.;
  cargoLock.lockFile = ./Cargo.lock;

  ANYRUN_FILES_FD_BINARY = lib.getExe pkgs.fd;

  meta = with lib; {
    description = "Anyrun plugin for searching through files, powered with `fd`";
    homepage = "https://gitlab.com/liferooter/anyrun-files";
    licenses = with licenses; [ gpl3Plus ];
  };
}
