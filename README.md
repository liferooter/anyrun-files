# Anyrun Files Plugin

[Anyrun](https://github.com/anyrun-org/anyrun) plugin for searching through files in home directory. Gets list of files using [fd](https://github.com/sharkdp/fd).

## Configuration

Can be configured though `<Anyrun configuration directory>/files.toml`.

Available options:
- `fd-binary`: overrides `fd` binary used to get list of files in home directory. Defaults to `"fd"`. Default can be overridden at build time using `ANYRUN_FILES_FD_BINARY` environment variable.
- `fd-args`: overrides arguments passed to `fd` to get list of all files in home directory. Defaults to `[]`.
- `matches-limit`: overrides matches limit. Defaults to 10.
