// SPDX-FileCopyrightText: 2024 Gleb Smirnov <glebsmirnov0708@gmail.com>
//
// SPDX-License-Identifier: GPL-3.0-or-later

use std::{
    fs::File,
    os::fd::AsFd,
    path::{Path, PathBuf},
    process::Command,
};

use abi_stable::std_types::{ROption, RString, RVec};
use anyrun_plugin::*;
use ashpd::desktop::open_uri::{OpenDirectoryRequest, OpenFileRequest};
use futures::TryFutureExt;
use fuzzy_matcher::{skim::SkimMatcherV2, FuzzyMatcher};
use itertools::Itertools;
use once_cell::sync::OnceCell;

#[derive(Debug, Clone, serde::Deserialize)]
#[serde(rename_all = "kebab-case", default)]
struct Config {
    fd_binary: String,
    fd_args: Vec<String>,
    matches_limit: usize,
}

impl Default for Config {
    fn default() -> Self {
        Self {
            fd_binary: std::option_env!("ANYRUN_FILES_FD_BINARY")
                .unwrap_or("fd")
                .into(),
            fd_args: Vec::new(),
            matches_limit: 10,
        }
    }
}

#[derive(Debug)]
struct State {
    files: Vec<String>,
    matches_limit: usize,
}

static STATE: OnceCell<State> = OnceCell::new();

#[init]
fn init(config_dir: RString) {
    fn warn_on_error(err: &impl std::error::Error) {
        eprintln!("[WARN] Failed to load anyrun-files config: {err}");
    }

    fn bail<E: std::error::Error, T>(message: &'static str) -> impl Fn(E) -> T {
        move |err| {
            eprintln!("[ERR] {message}: {err}");
            std::process::exit(1)
        }
    }

    let config_path = PathBuf::from(config_dir.to_string()).join("files.toml");
    let config_content = config_path
        .exists()
        .then(|| std::fs::read_to_string(config_path))
        .transpose()
        .inspect_err(warn_on_error)
        .ok()
        .flatten()
        .unwrap_or_default();
    let config: Config = toml::from_str(&config_content)
        .inspect_err(warn_on_error)
        .unwrap_or_default();

    let output = Command::new(config.fd_binary)
        .args(config.fd_args)
        .current_dir(std::env::var("HOME").unwrap_or_else(bail("Failed to get home directory")))
        .output()
        .unwrap_or_else(bail("Failed to get list of files in home directory"))
        .stdout;

    let files = String::from_utf8_lossy(&output)
        .lines()
        .map(String::from)
        .collect();

    STATE
        .set(State {
            files,
            matches_limit: config.matches_limit,
        })
        .expect("Init must be called only once");
}

#[info]
fn info() -> PluginInfo {
    PluginInfo {
        name: "Files".into(),
        icon: "system-file-manager".into(),
    }
}

#[get_matches]
fn get_matches(query: RString) -> RVec<Match> {
    if query.is_empty() {
        return RVec::new();
    }

    let state = STATE.get().expect("Plugin is not initialized");

    let matcher = SkimMatcherV2::default();
    state
        .files
        .iter()
        .filter_map(|path| {
            matcher
                .fuzzy_match(path, &query)
                .map(|score| (std::cmp::Reverse(score), path))
        })
        .take(100)
        .sorted_unstable()
        .take(state.matches_limit)
        .map(|(_, path)| Match {
            title: path.as_str().into(),
            description: ROption::RNone,
            use_pango: false,
            icon: ROption::RNone,
            id: ROption::RNone,
        })
        .collect()
}

#[handler]
fn handler(selection: Match) -> HandleResult {
    async_std::task::spawn::<_, Result<(), ()>>(
        async move {
            let path: &Path = selection.title.as_str().as_ref();
            let file = File::open(path)?;

            if path.is_dir() {
                OpenDirectoryRequest::default().send(&file.as_fd()).await
            } else {
                OpenFileRequest::default().send_file(&file.as_fd()).await
            }?;

            Ok(())
        }
        .map_err(|err: Box<dyn std::error::Error>| {
            eprintln!("[WARN] Failed to open file: {err}");
        }),
    );

    HandleResult::Close
}
